Feature:
    In order to gather information from respondents
    As admin API user
    I must be able to retrieve and edit questions and answer choices

    Scenario: Retrieve list of questions and choices on default language
        When I send a GET request to "/api/v1/questions"
        Then the response code should be 200
        And the response should contain json:
        """
        {
            "data": [
                {
                    "text": "What is the capital of Luxembourg ?",
                    "createdAt": "@datetime",
                    "choices": [
                        {
                          "text": "@string"
                        }
                    ]
                }
            ]
        }
        """

#    Scenario: Retrieve translated list of questions and choices
#        Given I add "Accept-Language" header with value "fr"
#        When I send a GET request to "/api/v1/questions"
#        Then the response code should be 200
#        And the response should contain json:
#        """
#        {
#          "data": [
#            {
#              "text": "Quelle est la capitale du Luxembourg ?",
#              "createdAt": "@datetime",
#              "choices": [
#                {
#                  "text": "string"
#                }
#              ]
#            }
#          ]
#        }
#        """

    Scenario: Add questions to questionnaire
        When I send a POST request to "/api/v1/questions" with body:
        """
        {
          "text": "string",
          "createdAt": "2021-10-23T09:13:36.621Z",
          "choices": [
            {
              "text": "string"
            }
          ]
        }
        """
        Then the response code should be 204
