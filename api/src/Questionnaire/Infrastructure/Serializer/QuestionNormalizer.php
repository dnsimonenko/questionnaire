<?php

declare(strict_types=1);

namespace Questionnaire\Infrastructure\Serializer;

use Questionnaire\Domain\Model\Question;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Webmozart\Assert\Assert;

class QuestionNormalizer implements ContextAwareNormalizerInterface
{
    public const APPLY_TRANSLATION = 'apply_translation';
    public const LOCALE = 'locale';

    private GoogleTranslate $translate;

    public function __construct(GoogleTranslate $translate)
    {
        $this->translate = $translate;
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof Question;
    }

    /**
     * @param Question $object
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        Assert::isInstanceOf($object, Question::class);
        if (!empty($context[self::APPLY_TRANSLATION]) &&
            !empty($context[self::LOCALE]) &&
            $context[self::APPLY_TRANSLATION] === true
        ) {
            $object = $object->applyTranslation($this->translate, $context[self::LOCALE]);
        }

        return $object->toArray();
    }
}
