<?php

declare(strict_types=1);

namespace Questionnaire\Infrastructure\Domain\Model;

use InvalidArgumentException;
use Questionnaire\Domain\Model\Question;
use Questionnaire\Domain\Model\QuestionRepository;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\SerializerInterface;
use Webmozart\Assert\Assert;

class JsonFileQuestionRepository implements QuestionRepository
{
    private Filesystem $filesystem;
    private SerializerInterface $serializer;

    private const SOURCE_FILE = '/srv/doc/questions.json';

    public function __construct(
        Filesystem $filesystem,
        SerializerInterface $serializer
    ) {
        $this->filesystem = $filesystem;
        $this->serializer = $serializer;
        if (!$this->filesystem->exists(self::SOURCE_FILE)) {
            throw new InvalidArgumentException('Missing source questions file');
        }
    }

    public function all(): array
    {
        $questions = [];
        $content = $this->getSourceFileContent();

        foreach ($content as $question) {
            $questions[] = Question::fromArray($question);
        }

        return $questions;
    }

    public function save(Question $question): void
    {
        $data = $this->all();
        $data[] = $question;
        $serialized = $this->serializer->serialize($data, 'json');

        $this->filesystem->dumpFile(self::SOURCE_FILE, $serialized);
    }

    private function getSourceFileContent(): array
    {
        $fileContent = file_get_contents(self::SOURCE_FILE);
        $decoded = json_decode($fileContent, associative: true);
        Assert::isArray($decoded, 'Wrong source questions file');

        return $decoded;
    }
}
