<?php

declare(strict_types=1);

namespace Questionnaire\Domain\Model;

interface QuestionRepository
{
    public function all(): array;

    public function save(Question $question): void;
}
