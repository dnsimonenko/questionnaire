<?php

declare(strict_types=1);

namespace Questionnaire\Domain\Model;

final class Choice
{
    private string $text;

    public function __construct(string $text)
    {
        $this->text = $text;
    }

    public function getText(): string
    {
        return $this->text;
    }
}
