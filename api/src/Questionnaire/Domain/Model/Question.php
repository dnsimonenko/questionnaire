<?php

declare(strict_types=1);

namespace Questionnaire\Domain\Model;

use DateTimeImmutable;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Webmozart\Assert\Assert;

final class Question
{
    /**
     * @var string
     */
    private string $text;

    /**
     * @var DateTimeImmutable
     */
    private DateTimeImmutable $createdAt;

    /**
     * @var Choice[]
     */
    private array $choices;

    /**
     * @param string $text
     * @param DateTimeImmutable $createdAt
     */
    public function __construct(string $text, DateTimeImmutable $createdAt)
    {
        $this->text = $text;
        $this->createdAt = $createdAt;
    }

    public function __clone()
    {
        $this->choices = [];
    }

    public function addChoice(Choice $choice): self
    {
        $this->choices[] = $choice;

        return $this;
    }

    public function applyTranslation(GoogleTranslate $translate, string $locale): self
    {
        $clone = clone $this;

        $clone->text = $translate->setTarget($locale)->translate($this->text);
        foreach ($this->choices as $choice) {
            $translated = $translate->setTarget($locale)->translate($choice->getText());
            $clone->addChoice(new Choice($translated));
        }


        return $clone;
    }

    public static function fromArray(array $data): self
    {
        Assert::string($data['text']);
        $createdAt = DateTimeImmutable::createFromFormat(DATE_RFC3339_EXTENDED, $data['createdAt']);

        $self = new self($data['text'], $createdAt);
        foreach ($data['choices'] as $choice) {
            Assert::string($choice['text']);
            $self->addChoice(new Choice($choice['text']));
        }

        return $self;
    }

    public function toArray()
    {
        $choices = [];
        /** @var Choice $choice */
        foreach ($this->choices as $choice) {
            $choices[] = ['text' => $choice->getText()];
        }

        return [
            'text' => $this->text,
            'createdAt' => $this->createdAt->format(DATE_RFC3339_EXTENDED),
            'choices' => $choices,
        ];
    }
}
