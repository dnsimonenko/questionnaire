<?php

declare(strict_types=1);

namespace Questionnaire\Presentation\Http\Api;

use Questionnaire\Application\Command\AddQuestionCommand;
use Questionnaire\Application\Command\AddQuestionCommandHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddQuestionAction
{
    /**
     * @Route("/v1/questions", methods={"POST"}, name="questionnaire/api/v1/add-question")
     */
    public function __invoke(Request $request, AddQuestionCommand $command, AddQuestionCommandHandler $handler)
    {
        $handler($command);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
