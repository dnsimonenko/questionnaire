<?php

declare(strict_types=1);

namespace Questionnaire\Presentation\Http\Api;

use Questionnaire\Application\Query\QuestionListQuery;
use Questionnaire\Application\Query\QuestionListQueryHandler;
use Questionnaire\Infrastructure\Serializer\QuestionNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class QuestionListAction
{
    /**
     * @Route("/v1/questions", methods={"GET"}, name="questionnaire/api/v1/question-list")
     */
    public function __invoke(
        Request $request,
        QuestionListQuery $query,
        QuestionListQueryHandler $handler,
        SerializerInterface $serializer
    ) {
        $questionList = $handler($query);
        $response = $serializer->serialize(
            [
                'data' => $questionList
            ],
            'json',
            [
                QuestionNormalizer::APPLY_TRANSLATION => true,
                QuestionNormalizer::LOCALE => $query->getAcceptedLanguage()
            ]
        );

        return new JsonResponse($response, json: true);
    }
}
