<?php

declare(strict_types=1);

namespace Questionnaire\Presentation\Http\Api;

use Questionnaire\Application\Query\QuestionListQuery;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class QuestionListQueryResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return QuestionListQuery::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        yield new QuestionListQuery($request->getPreferredLanguage());
    }
}
