<?php

declare(strict_types=1);

namespace Questionnaire\Presentation\View;

class ChoiceView
{
    private string $text;

    public function __construct(string $text)
    {
        $this->text = $text;
    }
}
