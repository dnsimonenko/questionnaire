<?php

declare(strict_types=1);

namespace Questionnaire\Presentation\View;

use DateTimeImmutable;

class QuestionListView
{
    private string $text;

    private DateTimeImmutable $createdAt;

    private array $choices;

    public function __construct(string $text, DateTimeImmutable $createdAt, array $choices)
    {
        $this->text = $text;
        $this->createdAt = $createdAt;
        $this->choices = $choices;
    }
}
