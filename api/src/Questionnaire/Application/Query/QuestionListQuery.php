<?php

declare(strict_types=1);

namespace Questionnaire\Application\Query;

class QuestionListQuery
{
    private string $acceptedLanguage;

    public function __construct(string $acceptedLanguage)
    {
        $this->acceptedLanguage = $acceptedLanguage;
    }

    public function getAcceptedLanguage(): string
    {
        return $this->acceptedLanguage;
    }
}
