<?php

declare(strict_types=1);

namespace Questionnaire\Application\Query;

use Questionnaire\Domain\Model\QuestionRepository;

class QuestionListQueryHandler
{
    private QuestionRepository $questionRepository;

    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    public function __invoke(QuestionListQuery $query)
    {
        return $this->questionRepository->all();
    }
}
