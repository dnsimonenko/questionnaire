<?php

declare(strict_types=1);

namespace Questionnaire\Application\Command;

use DateTimeImmutable;
use Questionnaire\Domain\Model\Choice;
use Questionnaire\Domain\Model\Question;
use Questionnaire\Domain\Model\QuestionRepository;

class AddQuestionCommandHandler
{
    private QuestionRepository $questionRepository;

    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    public function __invoke(AddQuestionCommand $command)
    {
        $createdAt = DateTimeImmutable::createFromFormat(DATE_RFC3339_EXTENDED, $command->getCreatedAt());
        $question = new Question($command->getText(), $createdAt);
        foreach ($command->getChoices() as $choice) {
            $question->addChoice(new Choice($choice->getText()));
        }

        $this->questionRepository->save($question);
    }
}
