<?php

declare(strict_types=1);

namespace Questionnaire\Application\Command;

class AddQuestionCommand
{
    private string $text;

    private string $createdAt;

    /**
     * @var ChoiceDto[]
     */
    private array $choices;

    /**
     * @param ChoiceDto[] $choices
     */
    public function __construct(string $text, string $createdAt, array $choices)
    {
        $this->text = $text;
        $this->createdAt = $createdAt;
        $this->choices = $choices;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @return ChoiceDto[]
     */
    public function getChoices(): array
    {
        return $this->choices;
    }
}
