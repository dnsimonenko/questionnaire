start:
	docker-compose up -d

stop:
	docker-compose stop

cli:
	docker-compose exec php sh

test:
	docker-compose exec php vendor/bin/behat

build:
	docker-compose build --force-rm
